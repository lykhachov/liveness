$(document).ready(() => {
    $('body').flowtype({
        minimum : 560,
        maximum : 1280,
        fontRatio: 31
    });

    const LivenessMenu = {
        nav: $('.header-nav'),
        menu: $('.header-nav > .menu'),
        openedSequence: [],

        open: function() {
            $(this.nav).animate({
                left: 0
            }, 200);
        },

        close: function() {
            $(this.nav).animate({
                left: '-100%'
            }, 200);
            this.openedSequence = [];
        },

        openSubMenu: function(e) {
            const el = $(e.currentTarget).siblings('.submenu');
            if (el.length > 0) {
                $(el).animate({
                    left: '0'
                }, 200);
                this.openedSequence.push(el);
            }
        },

        closeSubMenu: function() {
            const el = this.openedSequence.pop();
            $(el).animate({
                left: '-100%'
            }, 200);
        },

        addEvents: function() {
            const self = this;

            $('.menu-open').on('click', (e) => {
                e.preventDefault();
                e.stopPropagation();
                self.open();
            });

            $(window).on('click', (e) => {
                if($(e.target).closest(self.menu).length === 0) {
                    self.close();
                }
            });

            $(self.menu).on('click', 'span', (e) => {
                console.log(e);
                self.openSubMenu(e);
            });

            $('.submenu__back').on('click', () => {
                self.closeSubMenu();
            });
        }
    }
    LivenessMenu.addEvents();

    const footer = {
        pulledUp: false,
        pullUpButton: $('.pull-footer'),
        closeUpButton: $('.close-footer span'),
        header: $('header'),
        content: $('.content-wrapper'),
        footer: $('.footer'),
        scrollPos: null,

        init: function() {
            this.setUpPage();
            this.setUpEvents();
        },

        setUpPage: function() {
            if (this.pulledUp) return;

            let windowHeight = $(window).outerHeight();
            let headerHeight = this.header.outerHeight();
            let contentHeight = this.content.outerHeight();
            let footerHeight = this.footer.outerHeight();

            this.content.css('padding-top', headerHeight);
            this.footer.css('padding-top', contentHeight);

            let scroll = getCurrentScroll();
            if ( windowHeight + scroll > contentHeight ) {
                this.content.addClass('fixed');
                this.header.addClass('hidden');
            } else {
                this.content.removeClass('fixed');
                this.header.removeClass('hidden');
            }
        },

        setUpEvents: function () {
            let self = this;

            $(window).on('resize scroll', function() {
                self.setUpPage();
            });

            this.pullUpButton.on('click', () => {
                self.scrollPos = getCurrentScroll();
                self.pulledUp = true;
                self.closeUpButton.addClass('visible');
                self.content.css({'margin-top': -self.scrollPos, 'position': 'fixed'});

                self.footer.addClass('for-pull-up').css('pointer-events','all');
                self.footer.animate({
                    top: 0
                }, 400);
            });

            this.closeUpButton.on('click', () => {
                if (!self.pulledUp) return;
                self.pulledUp = false;
                self.footer.animate({
                    top: '100vh'
                }, 400, function() {
                    self.closeUpButton.removeClass('visible');
                    self.footer.removeClass('for-pull-up').css('top','').css('pointer-events','none');
                    self.content.css({'position': '', 'margin-top': ''});
                    $('html, body').scrollTop(self.scrollPos);
                    self.scrollPos = null;
                })
            });

        }
    }

   footer.init();
    // do it again as user scrolls or resizes

    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    $('.carousel-base').on('click', (e) => {
        e.preventDefault();
        const id = $(e.delegateTarget).attr('href');
        const bg = $(e.delegateTarget).data('bg');
        console.log(id)
        $.fancybox.open($('[data-fancybox="' + id + '"]'), {
            infobar: false,
            zoom: false,
            baseClass: bg,
            buttons: [
                "close"
            ],
            btnTpl: {
                arrowLeft:
                    '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
                    '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117.7 93.6" xml:space="preserve"><polygon points="46.1,93.8 0,46.9 46.1,0 52.5,6.3 17,42.4 117.7,42.4 117.7,51.4 17,51.4 52.5,87.4 "/></svg></div>' +
                    "</button>",

                arrowRight:
                    '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
                    '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117.7 93.6" xml:space="preserve"><polygon points="71.6,93.8 117.7,46.9 71.6,0 65.2,6.3 100.7,42.4 0,42.4 0,51.4 100.7,51.4 65.2,87.4 "/></svg></div>' +
                    "</button>"
            },
            afterShow: function(instance, slide) {
                const index = instance.currIndex + 1;
                const slides = instance.group.length
                $('.fancybox-caption').attr('data-count', index + ' / ' + slides);
            }
        })
    });

    $('.play').on('click', (e) => {
        e.preventDefault();
        const video = $(e.delegateTarget).closest('.video').find('video');
        video[0].play();
        $(e.delegateTarget).addClass('active');
        $(e.delegateTarget).siblings('.pause').removeClass('active');
    });

    $('.pause').on('click', (e) => {
        e.preventDefault();
        const video = $(e.delegateTarget).closest('.video').find('video');
        if (video[0].paused !== true && video[0].ended !== true) {
            video[0].pause();
            $(e.delegateTarget).addClass('active');
            $(e.delegateTarget).siblings('.play').removeClass('active');
        }
    });

    $('.video').each((i, el) => {
        if ($(el).find('video').attr('autoplay') !== undefined) {
            $(el).find('.play').addClass('active');
        } else {
            $(el).find('.pause').addClass('active');
        }
    });
})
